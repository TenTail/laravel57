<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class InvoicesPreMonthSheet implements FromView, WithTitle
{
    protected $year;
    protected $month;

    public function __construct($year, $month)
    {
        $this->year = $year;
        $this->month = $month;
    }

    public function view(): View
    {
        $invoices = collect();
        foreach (range(1, 30) as $i) {
            $invoice = new \stdClass;
            $invoice->number = sprintf('%08d', random_int(1, 99999999));
            $invoice->created_at = $this->year.'年'.$this->month.'月';

            $invoices->push($invoice);
        }

        $data = compact('invoices');

        return view('invoices.sheet', $data);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->year.'年'.$this->month.'月';
    }
}
