<?php

namespace App\Exports;

use App\Exports\Sheets\InvoicesPreMonthSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleSheetExport implements WithMultipleSheets
{
    use Exportable;

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $year = 2018;
        foreach (range(1, 12) as $month) {
            $sheets[] = new InvoicesPreMonthSheet($year, $month);
        }

        return $sheets;
    }
}
