<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use PDF;

class LaravelPdfController extends Controller
{
    public function view()
    {
        $users = User::all();

        $data = compact('users');

        // return view('laravel-pdf.users', $data);
        return PDF::loadView('laravel-pdf.users', $data)->stream('users.pdf');
    }
}
