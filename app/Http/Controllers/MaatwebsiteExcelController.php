<?php

namespace App\Http\Controllers;

use App\Exports\CollectionExport;
use App\Exports\MultipleSheetExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MaatwebsiteExcelController extends Controller
{
    public function collection()
    {
        return Excel::download(new CollectionExport, 'users.xlsx');
    }

    public function multipleSheet()
    {
        return (new MultipleSheetExport)->download('2018 Inovice.xlsx');
    }
}
