<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class FastExcelController extends Controller
{
    public function export()
    {
        $users = User::all();

        return (new FastExcel($users))->export('users.xlsx');
    }

    public function import()
    {
        $users = (new FastExcel)->import(public_path('users.xlsx'));

        dd($users);
    }
}
