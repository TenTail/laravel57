<table>
    <tr>
        <th>Number</th>
        <th>Created_at</th>
    </tr>
    @foreach ($invoices as $invoice)
        <tr>
            <td>{{ $invoice->number }}</td>
            <td>{{ $invoice->created_at }}</td>
        </tr>
    @endforeach
</table>
