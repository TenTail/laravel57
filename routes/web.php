<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// "maatwebsite/excel"
Route::get('maatwebsite/excel/collection', 'MaatwebsiteExcelController@collection');
Route::get('maatwebsite/excel/multipleSheet', 'MaatwebsiteExcelController@multipleSheet');

// "rap2hpoutre/fast-excel"
Route::get('fast-excel/export', 'FastExcelController@export');
Route::get('fast-excel/import', 'FastExcelController@import');

// "niklasravnsborg/laravel-pdf"
Route::get('pdf/view', 'LaravelPdfController@view');

// "milon/barcode"
Route::view('barcode', 'barcode.demo');
